import React, { Component } from 'react';
import { Input, } from 'react-materialize';
import styles from './SignUp.module.scss'
import LogIn from '../LogIn/LogIn'
import { connect } from 'react-redux';
import { login, resetPwd, createUser, openLogin, } from '../../store/AC/authentification'
import { emailValidator, phoneValidator, fieldNotEmpty, } from '../../utils/validators';
import Phone from '../../common/Phone';

class SignUp extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            showLoginForm: false,
            firstName: '',
            errorFirstName: false,
            lastName: '',
            errorLastName: false,
            email: '',
            errorEmail: false,
            companyName: '',
            errorCompany: false,
            phone: '',
            errorPhone: false,
            pwd: '',
            pwdConfirm: '',
        };

        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleSubmitLastName = this.handleSubmitLastName.bind(this);
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleSubmitCompany = this.handleSubmitCompany.bind(this);
        this.handleChangePwd = this.handleChangePwd.bind(this);
        this.handleSubmitPwdConfirm = this.handleSubmitPwdConfirm.bind(this);
      }
      
      
    handleChangeName(e){
        this.setState({ 
            firstName: e.target.value,
            errorFirstName: !fieldNotEmpty(e.target.value)
        });
    }
    handleSubmitLastName(e){
        this.setState({ 
            lastName: e.target.value,
            errorLastName:!fieldNotEmpty(e.target.value),
        });
    }
    handleChangeEmail(e){
        this.setState({ 
            email: e.target.value,
            errorEmail: !emailValidator(e.target.value),
            });
    }
    handleSubmitCompany(e){
        this.setState({ 
            companyName: e.target.value,
            errorCompany: !fieldNotEmpty(e.target.value),
        });
    }
    handleChangePhone = (value) => {
        this.setState({ 
            phone: value,
            errorPhone: !phoneValidator(value),
        });
    }
    handleChangePwd(e){
        this.setState({ pwd: e.target.value});
    }
    handleSubmitPwdConfirm(e){
        this.setState({ pwdConfirm: e.target.value});
    }
    
    createNewUser = () => {
        const { createUser, } = this.props;
        const { firstName, lastName, email, companyName, phone, errorFirstName,
            errorLastName,
            errorEmail,
            errorPhone,
            errorCompany, } = this.state;
        if(!errorFirstName && !errorLastName && !errorEmail && !errorPhone && !errorCompany &&
            firstName.length*lastName.length*email.length*companyName.length*phone.length*companyName.length>0){
            createUser({
                firstName, lastName, companyName, email, 
                partner: false,
                active: true,
                phone: phone

            });
        } else {
            this.setState({
                errorFirstName: !fieldNotEmpty(firstName),
                errorLastName: !fieldNotEmpty(lastName),
                errorEmail: !emailValidator(email),
                errorPhone: !phoneValidator(phone),
                errorCompany: !fieldNotEmpty(companyName),
            })

        }
    }

    login = () => {
        this.props.openLogin();
        this.setState({
            showLoginForm: !this.state.showLoginForm,
        })
    }


    static getDerivedStateFromProps(props, state){
        const { isLogin, userID, history, shouldChangePassword } = props;

        if(isLogin && userID!=='' && shouldChangePassword){
            history.push('/account');
        } 
        if(isLogin && userID!=='' && !shouldChangePassword){
            history.push('/dashboard');
        }
        return null;
    }
    render() {
        const { showLoginForm, errorFirstName, errorLastName, errorEmail, errorPhone, errorCompany, } = this.state;
        const { errorMessage } = this.props;
        return (
          <div className={styles.signup_wrapper+' row'}>
            {showLoginForm ? <LogIn close={this.login} {...this.props} /> : null}
            <div className={styles.topSquare}></div>
            <p className='col-md-8 offset-md-2 col-sm-10 offset-sm-1 big-size-font'>
                Get started with an Aila developer account. <br/> No credit card required.
            </p>
            
            <div className='col-md-8 offset-md-2'>
                <span className={errorFirstName ? styles.errorField : '' }>
                    <Input onChange={this.handleChangeName} s={6} label="* First Name" />
                </span>
                <span className={errorLastName ? styles.errorField : '' }>
                    <Input onChange={this.handleSubmitLastName} s={6} label="* Last Name" />
                </span>
            </div>     
        
            <div className={(errorEmail ? styles.errorField : '') + ' col-md-8 offset-md-2'}>
                <Input onChange={this.handleChangeEmail} s={12} label="* Email Address"  />
            </div>
        
            <div className={(errorCompany ? styles.errorField : '') + ' col-md-8 offset-md-2'}>
               <Input onChange={this.handleSubmitCompany} s={12} label="* Company"  />
            </div>
              <div className={(errorPhone ? styles.errorField : '') + ' col-md-8 offset-md-2'}>
                <Phone value={this.state.phone} placeholder="* Phone" onChange={this.handleChangePhone} />
              </div>
              <div className="col-md-8 offset-md-2">
                <div className={styles.error}>
                    {errorMessage}
                </div>
              </div>
            <div className={styles.get_started_block+' col-md-8 offset-md-2 col-sm-10 offset-sm-1'}>
                <div 
                    className={styles.blueBtn}
                    onClick={this.createNewUser}> Get Started </div>
                <p>By clicking the button, you agree to our <a href='#'>legal policies</a>.</p>
            </div>
        
            <div className='col-md-8 offset-md-2  col-sm-10 offset-sm-1'>
                <p>Already have account? 
                    <span 
                      className={styles.login}
                      onClick={this.login}
                      >Log in</span></p>
            </div>
          </div>
        );
    }
}


const mapStateToProps = (state) => {
    const { 
        visibleLoginForm, 
        isLogin, 
        userID, 
        shouldChangePassword,
        userCreated,
        resetSuccess,
        resetFail,
        errorMessage,
    } = state.authentification;
    return {
        visibleLoginForm, isLogin, userID, shouldChangePassword, userCreated,
        resetSuccess,
        resetFail,
        errorMessage
    };
};
  
const mapDispatchToProps = dispatch => ({
    login: (authData) => dispatch(login(authData)),
    resetPwd: (email) => dispatch(resetPwd(email)),
    createUser: (userData) => dispatch(createUser(userData)),
    openLogin: () => dispatch(openLogin())
});
  
export default connect( mapStateToProps, mapDispatchToProps)(SignUp);
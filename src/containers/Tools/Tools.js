import React, { Component } from 'react';
import ToolTile from '../../common/ToolTile/ToolTile';
import PageLayout from '../../common/PageLayout/PageLayout';
import demoApp from '../../img/tools/Demo App.png';
import demoBarcodes from '../../img/tools/Demo Barcodes.png';
import productApp from '../../img/tools/Aila Product App.png';
import ailaDiagnostic from '../../img/tools/Aila Diagnostics.png';
import installing from '../../img/tools/Installing.png';
import interactiveKios from '../../img/tools/Interactive Kiosk.png';
import mobileImager from '../../img/tools/Mobile Imager.png';

const tools = [
  {
    icon: demoApp,
    title: "Demo App v. 1.8.2",
    link: "../../files/181127_-_Aila_Demo_(PLS1.8.2).ipa",
    description: "Simple Demo App that can be loaded on to an Aila Interactive Kiosk or Mobile Imager to demonstrate barcode reading functionality ",
  },
  {
    icon: demoBarcodes,
    title: "Demo Barcodes for Alia Product App",
    link: "https://ailatech.com/wp-content/uploads/2018/10/Aila_Scan_Samples.pdf",
    description: "Barcodes to support scanning functionality of the AilaProduct app in the App Store. ",
  },
  {
    icon: productApp,
    title: "Aila Product App",
    link: "http://itunes.apple.com/us/app/ailaproduct/id1398361608",
    description: "AilaProduct is a demo app on the Apple App Store that can be used to turn your Interactive Kiosk into a functional product information kiosk. ",
  },
  {
    icon: ailaDiagnostic,
    title: "Aila Diagnostics",
    link: "https://files.ailatech.com/sharing/wGgG7eyB6",
    description: "Only to be used when instructed by the Aila Support team, this app can help verify if the Interactive Kiosk or Mobile Imager is functioning properly. "
  },
  {
    icon: installing,
    title: "Installing the Aila Demo Scanning App",
    link: "https://ailatech.com/tools/installing-the-aila-demo-app/",
    description: "Instructions on how to use iTunes to load the Demo App. ",
  },
  {
    icon: installing,
    title: "Installing the Aila Demo Diagnostics App",
    link: "https://ailatech.com/tools/installing-the-aila-diagnostics-app/",
    description: "Instructions on how to install the Aila Diagnostics App using iTunes. ",
  },
  {
    icon: interactiveKios,
    title: "Getting Started with your Interactive Kiosk",
    link: "https://ailatech.com/tools/interactive-kiosk-assembly/",
    description: "Instructions on how to set up your Interactive Kiosk. ",
  },  
  {
    icon: mobileImager,
    title: "Getting Started with your Mobile Imager",
    link: "https://ailatech.com/tools/getting-started-with-your-mobile-imager/",
    description: "Instructions on how to set up your Mobile Imager ",
  }
];

class Tools extends Component {
  render() {
    return (
      <PageLayout title="Tools">
          {tools.map(item => <ToolTile key={item.title} {...item} />)}
      </PageLayout>
    );
  }
}

export default Tools;

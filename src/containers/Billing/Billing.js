import React, { Component } from 'react';
import PageLayout from '../../common/PageLayout';
import PlanPanel from '../../blocks/PlanPanel';
import styles from './Biling.module.scss';
import BillingModal from '../../blocks/BillingModal';
import { Elements, StripeProvider } from 'react-stripe-elements';
import { connect } from 'react-redux';
import { billingAddSubscription } from '../../store/AC/billing';
import { dashboardGetAssignedKeycodes } from '../../store/AC/dashboard';

const stripeKey = process.env.REACT_APP_STRIPE_KEY;
const plans = [
  {
    title: "30 day free trial",
    description: "Try it risk-free",
    price: "$0.00",
    items: [
      "Try it risk-free", 
      "Upgrade to a paid tier at any time", 
      "Integrate scanning into your app via the Aila SDK"
    ],
    choose: 'trial'
  },
  {
    title: "Quarterly",
    description: "Minimize up-front costs",
    price: "$5.00",
    items: [
      "Minimize up-front costs", 
      "Flexibility to grow with your business", 
      "Includes unlimited scans"
    ],
    choose: 'quarterly'
  },
  {
    title: "Annual",
    description: "Save 10% with an annual payment",
    price: "$3.00",
    items: [
      "Save 10% with an annual payment", 
      "Ideal for enterprise deployments", 
      "Includes unlimited scans"
    ],
    choose: 'annual'
  }
];

class Billing extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      choose: null,
    }
  }
  componentDidMount() {
    this.props.getKeycodes();
  }

  openModal = (choose) => {
    this.setState({
      open: true,
      choose: choose,
    });
  }

  closeModal = () => {
    this.setState({
      open: false,
    });
  }

  render() {
    const { choose, open } = this.state;
    const { charge, charged, user, disabled } = this.props;
    return (
      <StripeProvider apiKey={stripeKey}>
        <PageLayout title="Available Plans" rowClassName="justify-content-center">
          {open ? <Elements>
            <BillingModal 
              charged={charged} 
              charge={charge} 
              choose={choose} 
              open={open} 
              onClose={this.closeModal} 
              user={user}
            /> 
          </Elements> : null}
          {plans.map(plan => <PlanPanel disabled={disabled} action={this.openModal} key={plan.title} {...plan} />)}
        </PageLayout>
      </StripeProvider>
    );
  }
}

const mapStateToProps = (state) => {
  const { fullUserInfo: { user, roles } } = state.authentification;
  const { assignedKeycodes } = state.dashboard;

  const disabled = assignedKeycodes.length > 0 && !roles.includes('Partner') ? true : false;

  return {
    user,
    roles,
    disabled
  };
};

const mapDispatchToProps = dispatch => ({
  addSubscription: (data) => dispatch(billingAddSubscription(data)),
  getKeycodes: () => dispatch(dashboardGetAssignedKeycodes())
});

export default connect(mapStateToProps, mapDispatchToProps)(Billing);

import React, { Component } from 'react';
import { Input, } from 'react-materialize';
import styles from './Account.module.scss';
import { changePassword, updateUserInfo, } from '../../store/AC/authentification';
import { connect, } from 'react-redux';
import { showModalWindow } from '../../store/AC/manageModalWindow';
import { DELETE_USER_SELF } from '../../store/constants/usersManage';
import { hideDelete } from '../../utils/helpers';


class Account extends Component {

    constructor(props) {
        super(props);
        this.state = {
            oldPassword: '',
            newPassword: '',
            firstName: '',
            lastName: '',
            companyName: '',
            phone: '',
            canEdit: false,
        };
          
        this.handleNewPwd = this.handleNewPwd.bind(this);
        this.handleOldPwd = this.handleOldPwd.bind(this);
        this.handleFirstName = this.handleFirstName.bind(this);
        this.handleLastName = this.handleLastName.bind(this);
        this.handleCompany = this.handleCompany.bind(this);
        this.handlePhone = this.handlePhone.bind(this);
      }

    handleNewPwd(e){
        this.setState({
            newPassword: e.target.value
        })
    }
    handleOldPwd(e){
        this.setState({
            oldPassword: e.target.value
        })
    }
    changePwd = () => {
       const { newPassword, oldPassword, } = this.state;
       this.props.changePassword({ newPassword, oldPassword });
    }
    handleFirstName = (e) => {
        this.setState({
            firstName: e.target.value
        })
    }
    handleLastName = (e) => {
        this.setState({
            lastName:  e.target.value
        })
    }
    handleCompany = (e) => {
        this.setState({
            companyName:  e.target.value
        })
    }
    handlePhone = (e) => {
        this.setState({
            phone:  e.target.value
        })
    }
    showEditForm = () => {
        this.setState({
            canEdit: !this.state.canEdit,
        })
    }
    updateUser = () => {
        const { fullUserInfo, fullUserInfo:{user:{id}}, updateUserInfo } = this.props;
        const  {firstName, lastName, companyName, phone,} = this.state;
        let editedUser = Object.assign( {},
            fullUserInfo.user,
            {firstName},
            {lastName},
            {companyName},
            {phone: phone.toString()});
        updateUserInfo(editedUser, id);
        this.setState({
            canEdit: false
        })
    }
    componentWillMount(){
        if(this.props.fullUserInfo && this.props.fullUserInfo.user){
            const {firstName, lastName, companyName, phone,} = this.props.fullUserInfo.user;
            this.setState({
                firstName, lastName, companyName, phone,
            });
        }

    }

    deleteUserSelf = () => {
        this.props.showModalWindow({
            openModal: true,
            modalTitle: 'Delete your data',
            modalDescription: `Are you sure you would like to delete your data?`,
            action: DELETE_USER_SELF
        })
    }
    render() {
        const { fullUserInfo:{ user, roles} } = this.props;
        const { canEdit } = this.state;
        return (
            <div className={styles.accountPage+' row'}>
                <div className='big-size-font col-md-11 md-offet-1'>
                    <span>Account page</span>
                    { !hideDelete(roles) ?
                    <div className={styles.deleteBtn}>
                        <button className='btn btn-danger btn-sm'
                            onClick={this.deleteUserSelf}>Delete account</button>
                    </div> : null }
                </div>
                <div className={'col-md-6 '+(canEdit ? styles.userEditInfo : styles.userInfo)}>
                    <div className='middle-size-font'>User information</div>
                    <div className='btn btn-primary' onClick={this.showEditForm}>Edit</div>
                    <div>
                        <span>
                            Name: 
                            {user.firstName}
                        </span>
                        <div>
                        <Input
                            s={6}
                            defaultValue={user.firstName}
                            label="First name"
                            onChange={this.handleFirstName} />
                        </div>
                    </div>
                    <div>
                        <span>
                            Last Name: 
                            {user.lastName}
                        </span>
                        <div>
                        <Input
                            s={6}
                            defaultValue={user.lastName}
                            label="Last name"
                            onChange={this.handleLastName} />
                        </div>
                    </div>
                    <div> 
                        <span>
                            Company:
                            {user.companyName}
                        </span>
                        <div>
                        <Input
                            s={6}
                            defaultValue={user.companyName}
                            label="Company"
                            onChange={this.handleCompany} />
                        </div>
                    </div>
                    <div>
                        <span>
                            Phone: 
                            {user.phone}
                        </span>
                        <div>
                        <Input
                            s={6}
                            defaultValue={user.phone}
                            label="Phone"
                            onChange={this.handlePhone} />
                        </div>
                    </div>
                    {canEdit ? <div 
                        className='btn btn-save col-md-12'
                        onClick={this.updateUser}>SAVE</div>:null}
                    
                </div>

                <div className='col-md-6'>
                    <span className='middle-size-font'>Reset my password</span>
                    <Input
                        s={12}
                        label="Old password"
                        type="password"
                        onChange={this.handleOldPwd} />
                    <Input
                        s={12}
                        label="New password"
                        type="password"
                        onChange={this.handleNewPwd} />
                    <button 
                        className='btn btn-success col-md-12'
                        onClick={this.changePwd}>OK</button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const { fullUserInfo, fullUserInfo:{user}} = state.authentification;
    return {
        fullUserInfo, user,
    };
};
  
const mapDispatchToProps = dispatch => ({
    changePassword: (passwords) => dispatch(changePassword(passwords)),
    updateUserInfo: (userData, id) => dispatch(updateUserInfo(userData, id)),
    showModalWindow: data => dispatch(showModalWindow(data))
});
  
export default connect(mapStateToProps, mapDispatchToProps)(Account);
import React, { Component } from 'react';
import { connect, } from 'react-redux';
import styles from './ManageKeycodes.module.scss';
import KeycodeItem from '../../blocks/KeycodeItem/KeycodeItem';
import EditKeycode from '../../blocks/EditKeycode/EditKeycode';
import { editKeycodeById, getAllKeycodes, addNewKeycode, saveEditKeycode, setDeleteKeycodeId, } from '../../store/AC/keycodesManage' 
import { showModalWindow, } from '../../store/AC/manageModalWindow';
import { getAllUsers } from '../../store/AC/usersManage';
import { ADMIN } from '../../store/constants/main';

const excludeAdmin = (users) => {
    return users.filter(item => {
        let result = true;
        item.roles && item.roles.forEach(element => {
            if(element.name === ADMIN) {
                result = false;
                return;
            }
        })
        return result;
    });
}

class ManageKeycodes extends Component {
    componentDidMount = () => {
      this.props.getAllKeycodes();
      this.props.getAllUsers();
    };
    
    showAddKeycodeform = () => {
        this.props.editKeycodeById('addNew')
    }
    saveNewKeycode = (data) => {
        this.props.addNewKeycode(data);
    }
    
    getKeycodes = () => {
        return this.props.keycodes.map(keycode => 
            <KeycodeItem 
                keycodeData={keycode} 
                key={keycode.id} 
                {...this.props} 
                userByEmail={this.props.userByEmail || keycode.customer }
                deleteKeycode={this.deleteKeycode}
            />
        )
    }

    saveEditKeycode = (data) => {
        this.props.saveEditKeycode(this.props.editableKeycodeId, data);
    }

    deleteKeycode = (id) => {
        this.props.deleteKeycodeById(id);
    }

    render() {
        const { editableKeycodeId } = this.props;
        
        return (
            <div className={styles.keycodesPage + ' row'}>
                <p className='col-lg-10 col-md-10 offset-lg-1 offset-md-1 big-size-font'>Manage keycodes</p>
                
                <div className='col-lg-10 col-md-10 offset-lg-1 offset-md-1'> 
                    {
                        editableKeycodeId==='addNew' ? 
                        <EditKeycode  {...this.props} saveBtn={this.saveNewKeycode} />
                    :<div className={styles.editButton+' btn  btn-primary'} onClick={this.showAddKeycodeform}> Add new keycode</div>}
                </div>
                
                <div className={styles.usersList + ' col-lg-10 offset-lg-1 col-md-12'}>
                    <div className={styles.usersListTitle}>
                        <span>User email</span>
                        <span className={styles.keycode}>Keycode</span>
                        <span>Start date</span>
                        <span>Expiration date</span>
                        <div>Description</div>
                        <div>Actions</div>
                    </div>
                    {this.getKeycodes()}
                </div>
                
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const { editableKeycodeId, keycodes, errorMessage } = state.keycodesManage;
    const { users } = state.usersManage;
    return {
        editableKeycodeId,
        keycodes,
        users: excludeAdmin(users),
        errorMessage
    };
};
  
const mapDispatchToProps = dispatch => ({
    editKeycodeById: id => dispatch(editKeycodeById(id)),
    showModalWindow: modalData => dispatch(showModalWindow(modalData)),
    getAllKeycodes: () => dispatch(getAllKeycodes()),
    addNewKeycode: data => dispatch(addNewKeycode(data)),
    saveEditKeycode: (id, data) => dispatch(saveEditKeycode(id, data)),
    deleteKeycodeById: (id) => dispatch(setDeleteKeycodeId(id)),
    getAllUsers: () => dispatch(getAllUsers())
});
  
export default connect(mapStateToProps, mapDispatchToProps)(ManageKeycodes);
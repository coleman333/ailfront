import React, { Component } from 'react';
import styles from './Downloads.module.scss'
import data from '../../data/downloads.json';
import { NavLink } from 'react-router-dom';

class Downloads extends Component {
  render() {
    return (
      <div className={styles.downloadsPage + ' row'}> 
        <p className='col-lg-11 offset-lg-1 big-size-font'>Downloads</p>
        <p className={styles.pageDescription + ' col-lg-11 offset-lg-1 col-md-12'}>
            You can access your license keys in the <NavLink to="/dashboard" activeClassName={styles.activeLink}>License Keys</NavLink> section.
        </p>

        <div className='col-lg-11 offset-lg-1 col-md-12'>
            {data.map(item => 
              <div key={item.title}>
                  <h2 className='middle-size-font'>{item.title}</h2>
                  <table className={styles.downloadsTables + " table table-hover"}>
                  <tbody>
                    {item.links.map(link => 
                      <tr key={link.version}>
                        <td>{link.name}</td>
                        <td>{link.version}</td>
                        <td><a target="_blank" rel="noopener noreferrer" href={link.notes}>release notes</a></td>
                        <td><a target="_blank" rel="noopener noreferrer" href={link.download}>download</a></td>
                      </tr>
                    )}
                    <tr>
                      <td colSpan='4' className={styles.lastLine}>
                        <span>show more versions</span>
                      </td>
                    </tr>
                  </tbody>
                  </table>
              </div>
            )}
        </div>
      </div>
    );
  }
}

export default Downloads;

import React from 'react';
import styles from './Phone.module.scss';
import PhoneInput from 'react-phone-number-input';
import 'react-phone-number-input/style.css'

const Phone = ({ value, onChange, placeholder }) => {
    return (
        <div className={styles.container}>
              <PhoneInput
                    placeholder={placeholder}
                    value={ value }
                    onChange={onChange}
                />
        </div>
    );
}

export default Phone;
import React from 'react';
import styles from './Spinner.module.scss';

const Spinner = ({ className }) => {
   return (
     <div className={styles.preloader + ' ' + className} />
    );
};

export default Spinner;

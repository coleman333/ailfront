import React from 'react';
import styles from './PageLayout.module.scss';
import cn from "classnames";

const PageLayout = ({title, children, className, rowClassName}) => {
    return (
        <div className={cn(styles.container, 'container-fluid', className)}>
            <p className={cn(styles.title, 'big-size-font')}>{title}</p>
            <div className={cn("row", rowClassName)}>
                {children}
            </div>
        </div>
    );
}

export default PageLayout;
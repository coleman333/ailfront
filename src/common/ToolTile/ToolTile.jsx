import React from 'react';
import styles from './ToolTile.module.scss';
import cn from "classnames";

const ToolTile = ({ icon, link, title, description }) => {
    return (
        <div className={cn('col-lg-3 col-md-3 col-sm-3')}>
            <div className={styles.container}>
                <div>
                    <img src={icon} />
                </div>
                <a target="_blank" rel="noopener noreferrer" href={link}>{title}</a>
                <p>{description}</p>
            </div>
        </div>
    );
}

export default ToolTile;
import React, { Component } from 'react';
import Header from './blocks/Header/Header';
import Footer from './blocks/Footer/Footer';
import SignUp from '../src/containers/SignUp/SignUp';
import Dashboard from '../src/containers/Dashboard/Dashboard';
import Downloads from '../src/containers/Downloads/Downloads';
import Documentation from '../src/containers/Documentation/Documentation';
import Tools from '../src/containers/Tools/Tools';
import Billing from '../src/containers/Billing/Billing';
import Account from '../src/containers/Account/Account';
import ManageUsers from '../src/containers/ManageUsers/ManageUsers';
import ManageKeycodes from '../src/containers/ManageKeycodes/ManageKeycodes';
import ResetPassword from '../src/containers/ResetPassword/ResetPassword';
import RegistrationConfirmation from '../src/containers/RegistrationConfirmation/RegistrationConfirmation';
import { showInformModal, } from './store/AC/manageModalWindow';
import { connect, } from 'react-redux'
import { Router, Route } from "react-router-dom";
import ConfirmModalWindow from './containers/ConfirmModalWindow/ConfirmModalWindow';
import InfoModal from './blocks/InfoModal/InfoModal';
import history from './utils/history';

class App extends Component {

  render() {
		const {  isLogin, userID, fullUserInfo, modalData, showInformModal,} = this.props;
		let token = localStorage.getItem('id');
		let auth = false;
		let pwdChange = false;
		if(fullUserInfo && fullUserInfo.shouldChangePassword){
			pwdChange = true;
		}
		if(isLogin && userID!=='') {
			auth = true;
		}
		
		return (
			<Router history={history}>
		  <div>
		    <Header />
		    <div className='mainContainer'>
		       
						<Route path="/confirm-registration" component={RegistrationConfirmation} />
					{!auth ? <Route exact path="/" component={SignUp} /> : 
						<div>
							{pwdChange ? 
								<Route exact path="/" component={Account} />:
								<Route exact path="/" component={Dashboard} />
							}
							<Route path="/account" component={Account} />
							<Route path="/dashboard" component={Dashboard} />
							<Route path="/downloads" component={Downloads} />
							<Route path="/documentation" component={Documentation} />
							<Route path="/tools" component={Tools} />
							<Route path="/billing" component={Billing} />
							<Route path="/manage-users" component={ManageUsers} />
							<Route path="/manage-keycodes" component={ManageKeycodes} />
							<Route path={'/auth/reset/'+token} component={ResetPassword} />
						</div>
					}

		      <ConfirmModalWindow />
					<InfoModal  modalData={modalData} showInformModal={showInformModal} />
		      <Footer />
		    </div>
		  </div>
      </Router> 
    );
  }
}

const mapStateToProps = (state) => {
		const { isLogin, userID, fullUserInfo, newUserCreated,} = state.authentification;
		const { openModal, modalTitle, modalDescription, } = state.manageInfoModal
		const modalData = { openModal, modalTitle, modalDescription };
    return {
        isLogin, userID, fullUserInfo, newUserCreated, modalData,
    };
};

const mapDispatchToProps = dispatch => ({
  showInformModal: modalData => dispatch(showInformModal(modalData)),
});
  
export default connect(mapStateToProps, mapDispatchToProps)(App);
import React from 'react';
import styles from './GetStartedTile.module.scss';
import icon_1 from '../../img/icon_t.png';
import icon_2 from '../../img/icon_l.png';
import icon_3 from '../../img/icon_f.png';
import close_icon from '../../img/close.png';
import { NavLink } from 'react-router-dom';

const GetStartedTile = ({ switchVisibleRightBlock}) => {
    return (
        <div>
            <p className={styles.title+' middle-size-font'}>Getting Started with Aila
                <span>
                    <span 
                        className={styles.close}
                        onClick={switchVisibleRightBlock}><img src={close_icon} alt='close-icon' /></span>
                        <span>don't show <span className={styles.dontshow}>  me this again</span></span>
                </span>
            </p>
            

            <div className={styles.startedInfoItem}>
                <img src={icon_1} alt='alt_text' />
                <div>
                    <span>Manage your account</span>
                    <p>Manage the details of your Aila account</p>
                </div>

                <NavLink to='account'>
                   View Account Settings
                </NavLink>
            </div>

            <div className={styles.startedInfoItem}>
                <img src={icon_2} alt='alt_text' />
                <div>
                    <span>Downloads</span>
                    <p>Download our latest SDK for your project</p>
                </div>
                <NavLink to='/downloads'>
                    Go To Downloads</NavLink>
            </div>

            <div className={styles.startedInfoItem}>
                <img src={icon_3} alt='alt_text' />
                <div>
                    <span>Get Started</span>
                    <p>View our documentation to assist with integration</p>
                </div>
                <NavLink to='/documentation/welcome'>
                    View Documentation
                </NavLink>
            </div>                   

        </div>
    );
}

export default GetStartedTile;
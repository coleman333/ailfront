import React, { Component } from 'react';
import styles from './EditKeycode.module.scss';
import DatePicker from "react-datepicker";
import { Input } from 'react-materialize';
import Autosuggest from 'react-autosuggest';
import "react-datepicker/dist/react-datepicker.css";
import 'react-datepicker/dist/react-datepicker-cssmodules.css';
import { generateKeycode } from '../../utils/keycode';
import IsolatedScroll from 'react-isolated-scroll';

function renderSuggestionsContainer({ containerProps, children }) {
  const { ref, ...restContainerProps } = containerProps;
  const callRef = isolatedScroll => {
    if (isolatedScroll !== null) {
      ref(isolatedScroll.component);
    }
  };

  return (
    <IsolatedScroll ref={callRef} {...restContainerProps}>
      {children}
    </IsolatedScroll>
  );
}

const getSuggestionValue = suggestion => suggestion.email;
const renderSuggestion = suggestion => (
    <div>
      {suggestion.email}
    </div>
);

const alwaysTrue = () => true;

class EditKeycode extends Component {

    constructor(props) {
      super(props);
      const { keycodeData } = this.props;
      const { code, startDate, expirationDate, customer, description } = keycodeData ? keycodeData : {};

      this.state = {
          form: {
              code: code ? code : generateKeycode(),
              startDate: startDate ? startDate : new Date(), 
              expirationDate: expirationDate ? expirationDate : new Date(),
              customerId: customer ? customer.email : '',
              description: description ? description : 'Standard Plan'
          },
          suggestions: this.props.users
      };
    }
    
   
    cancel = () => {
        this.props.editKeycodeById(null);
    }
    save = (e) => {
        e.preventDefault();

        const data = { ...this.state.form };
        data.customerId = this.getCustomerId(this.state.form.customerId);
        this.props.saveBtn(data);
    }

    handleChange = (e) => {
        const { target } = e;
        const value = target.type === 'checkbox' ? target.checked : target.value;

        this.setState({
            ...this.state,
            form: {
                ...this.state.form,
                [e.target.name]: value
            }
        });
    }

    handleChangeDate = (name, date) => {
        this.setState({
            ...this.state,
            form: {
                ...this.state.form,
                [name]: date
            }
        });
    }

    getCustomerId = (value) => {
        let customerId;
        this.props.users.forEach(element => {
            if(element.email === value) {
                customerId = element.id;
                return;
            }
        });

        return customerId;
    }


    getSuggestions = value => {
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;

        if(inputValue === '') {
            return this.props.users;
        }
      
        return inputLength === 0 ? [] : this.props.users.filter(lang =>
          lang.email.toLowerCase().slice(0, inputLength) === inputValue
        );
      };

    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
          suggestions: this.getSuggestions(value)
        });
    };
    
    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    handleChangeUserEmail = (event, { newValue }) => {
        this.setState({
            form: {
                ...this.state.form,
                customerId: newValue
            }
        })
    }

    storeInputReference = autosuggest => {
        if (autosuggest !== null) {
          this.input = autosuggest.input;
        }
    };

    render() {
        const { errorMessage } = this.props;
        const { code, startDate, expirationDate, description, customerId } = this.state.form;

        const inputProps = {
            placeholder: 'User email',
            value: customerId,
            onChange: this.handleChangeUserEmail
        };

      return (
          <div className={styles.editKeycodeBlock}>
              <div className={styles.dataInput}>
                <div className="input-field">
                    <Autosuggest 
                        suggestions={this.state.suggestions}
                        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                        getSuggestionValue={getSuggestionValue}
                        renderSuggestion={renderSuggestion}
                        inputProps={inputProps}
                        shouldRenderSuggestions={alwaysTrue}
                        renderSuggestionsContainer={renderSuggestionsContainer}
                    />
                </div>
                <div className={styles.keycodeInput}>
                    <Input name="code" onChange={this.handleChange} label="Keycode" defaultValue={code} />
                </div>
                <div className={styles.datePicker}>
                    <DatePicker
                        selected={new Date(startDate)}
                        onChange={(date) => this.handleChangeDate('startDate', date)}
                    />
                </div>
                <div className={styles.datePicker}>
                    <DatePicker
                        selected={new Date(expirationDate)}
                        onChange={(date) => this.handleChangeDate('expirationDate', date)}
                    />
                </div>
                <div className={styles.keycodeInput}>
                    <Input maxLength="25" name="description" onChange={this.handleChange} label="Description" defaultValue={description} />
                </div>
                <div style={{'width': '20%'}}></div>
                
              </div>
              <div className={styles.error}>
                  {errorMessage}
              </div>
              <div className={styles.saveButton+' btn btn-success'} onClick={this.save}>Save</div>
              <div className={styles.cancelButton+' btn btn-success'} onClick={this.cancel}>Cancel</div>
          </div>
      );
    }
}

export default EditKeycode;

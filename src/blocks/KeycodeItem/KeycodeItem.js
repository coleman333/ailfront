import React, { Component } from 'react';
import styles from './KeycodeItem.module.scss';
import EditKeycode from '../EditKeycode/EditKeycode'
import { DELETE_KEYCODE } from '../../store/constants/keycodesManage';
import { formatDate } from '../../utils/helpers';

class KeycodeItem extends Component {

    deleteKeycode = () => {
        const { keycodeData: { id, customer: { email } }, showModalWindow } = this.props;
        showModalWindow({
            openModal: true,
            modalTitle: 'Delete Keycode',
            modalDescription: `Are you sure you would like to delete keycode from user ${email} ?`,
            action: DELETE_KEYCODE
        });
        this.props.deleteKeycode(id);
    }

    editKeycode = () => {
        this.props.editKeycodeById(this.props.keycodeData.id)
    }

    saveEditKeycode = (data) => {
        this.props.saveEditKeycode(this.props.keycodeData.id, data);
    }
    
    render() {
        const { editableKeycodeId, keycodeData } = this.props;
        const { customer: { email }, id, code, startDate, expirationDate, description } = keycodeData;
        if(editableKeycodeId === id){ 
            return <EditKeycode
                saveBtn={this.saveEditKeycode}
                keycodeData={keycodeData}
                {...this.props}
            />}
        return (
            <div className={styles.keycodeItem}>
                <span>{email}</span>
                <span className={styles.keycodeValue}>{code}</span>
                <span>{formatDate(startDate)}</span>
                <span>{formatDate(expirationDate)}</span>
                <div>{description}</div>
                <div>
                    <div 
                        className='btn-sm btn-primary'
                        onClick={this.deleteKeycode}>Delete</div>
                    <div
                        className='btn-sm btn-secondary'
                        onClick={this.editKeycode}>Edit</div>
                </div>
            </div>
        );
  }
}

export default KeycodeItem;
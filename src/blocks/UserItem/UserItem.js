import React, { Component } from 'react';
import styles from './UserItem.module.scss';
import EditUser from '../EditUser/EditUser';
import { INACTIVE_USER } from '../../store/constants/usersManage';
import { hideDelete } from '../../utils/helpers';

class UserItem extends Component {

    
    deleteUser = () => {
        const { id, email } = this.props.userData;
        this.props.makeInactiveUserById(id);
        this.props.showModalWindow({
            openModal: true,
            modalTitle: 'Delete User',
            modalDescription: `Are you sure you would like to delete customer ${email}?`,
            action: INACTIVE_USER
        })
    }
    editUser = () => {
        const { userData, editUserById, } = this.props;
        editUserById(userData.id)
    }
    resetUserPwd = () => {

    }
    updateUser = (data) => {
        this.props.save(data);
    }

    render() {
        const { userData, editableUserId } = this.props;
        const { id, firstName, lastName, phone, email, partner, roles} = userData;
        if(editableUserId === id){ 
            return <EditUser
                saveBtn={this.updateUser}
                userData={userData}
                {...this.props}
            />}
        return (
            <div className={styles.userItem}>
                <span>{firstName}</span>
                <span>{lastName}</span>
                <span className={styles.email}>{email}</span>
                <span className={styles.phone}>{phone}</span>
                <span>{partner ? 'partner' : 'not partner'}</span>
                <div>
                    { !hideDelete(roles) ?
                        <div 
                            className='btn-sm btn-primary'
                            onClick={this.deleteUser}>Delete</div>
                        : null
                    }
                    <div
                        className='btn-sm btn-secondary'
                        onClick={this.editUser}>Edit</div>
                    <div 
                        className='btn-sm btn-info'
                        onClick={this.resetUserPwd}>Reset</div>
                </div>
            </div>
        );
  }
}

export default UserItem;
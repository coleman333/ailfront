import React from 'react';
import styles from './ActivationTile.module.scss';
import { formatDate } from '../../utils/helpers';

const ActivationTile = ({ keycode }) => {
    const activations = keycode.activations.length;
    const remaining = 100 - keycode.activations.length;
    return (
        <div>
            <p className={styles.title+' middle-size-font'}>Activations</p>
            <p>{keycode.code}</p>
            <div className={styles.activationCount}>
                <span>{activations}</span> Activations
            </div>

            <div>
                <span>{remaining > 0 ? remaining : 0}</span> SoftScan Activations Remaining
            </div>

            <div className={styles.activationSummary}>
                <p>
                    <strong>Current Plan:</strong> {keycode.description} (expires {formatDate(keycode.expirationDate)}) <a href='#'>view terms</a>
                </p>
                <p>
                    <strong>Platform:</strong> iOS
                </p>
                <p>
                    <strong>Max Number of SoftScan Activations:</strong> 100
                </p>
            </div>
            <div className={styles.blueBtn}>
                Upgrade Licenses
            </div>
            <div className={styles.blueBtn}>
                Cancel Subscription
            </div>
        </div>
    );
}

export default ActivationTile;
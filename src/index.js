import React from 'react';
import ReactDOM from 'react-dom';
import './fonts/import-fonts.css';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux'; 
import store from './store';
import { PersistGate } from 'redux-persist/integration/react';

const storeConfig = store();

ReactDOM.render(
    <Provider store={storeConfig.store}>
        <PersistGate loading={null} persistor={storeConfig.persistor}>
            <App />
        </PersistGate>
    </Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();

export const SUCCESS = '_SUCCESS';
export const FAIL = '_FAIL';
export const AUTHORIZATION_REQUIRED = 'AUTHORIZATION_REQUIRED';
export const ADMIN = 'Admin';
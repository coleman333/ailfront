export const SHOW_MODAL_WINDOW = 'SHOW_MODAL_WINDOW';
export const CLOSE_MODAL_WINDOW = 'CLOSE_MODAL_WINDOW';
export const CONFIRM_MODAL_ACTION = 'CONFIRM_MODAL_ACTION';
export const SHOW_INFO_MODAL = 'SHOW_INFO_MODAL';
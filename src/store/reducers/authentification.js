import {
    RESET_PASSWORD,
    SET_TEMPORARY_TOKEN,
    CREATE_NEW_USER,
    LOGIN,
    LOGOUT,
    CHANGE_PASSWORD,
    UPDATE_USER,
    OPEN_LOGIN,
} from '../constants/authentification';
import { SUCCESS, AUTHORIZATION_REQUIRED, FAIL } from '../constants/main';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { DELETE_USER_SELF } from '../constants/usersManage';
import history from '../../utils/history';


let initialState = {
    isLogin: false,
    userID: '',
    isTempID: false,
    shouldChangePassword: false,
    fullUserInfo: {},
    newUserCreated: false,
    resetSuccess: false,
    resetError: false,
    errorMessage: null
};

 const user = function authentification(state = initialState, action) {
    switch (action.type) {
        case LOGIN + SUCCESS:
            return{
                ...state,
                isLogin: true,
                userID: action.payload.id,
                shouldChangePassword: action.payload.shouldChangePassword,
                fullUserInfo: action.payload,
            }
        case LOGOUT + SUCCESS:
            return {
                isLogin: false,
                userID: '',
            }
        case RESET_PASSWORD + SUCCESS:
            return {
                ...state,
                isTempID: false,
                resetSuccess: true,
                resetFail: false
            }
        case RESET_PASSWORD + FAIL:
            return {
                ...state,
                resetFail: true,
                resetSuccess: false
            }
        case SET_TEMPORARY_TOKEN:
            return {
                ...state,
                isTempID: true,
            }
        case CHANGE_PASSWORD + SUCCESS:
            return {
                ...state,
                shouldChangePassword: false
            }
        case CREATE_NEW_USER:
            return { ...state };
        case UPDATE_USER + SUCCESS:
            let newFullUserInfo = state.fullUserInfo;
            newFullUserInfo.user = action.payload;
            return {
                ...state,
                fullUserInfo: newFullUserInfo
            }
        case CREATE_NEW_USER + SUCCESS:
            history.push('/confirm-registration');
            return {
                ...state,
                newUserCreated: true,
                errorMessage: null,
            }
        case AUTHORIZATION_REQUIRED:
        case DELETE_USER_SELF + SUCCESS:
            return {
                isLogin: false,
                userID: '',
                isTempID: false,
                shouldChangePassword: false,
                fullUserInfo: {},
                newUserCreated: false
            }
        case OPEN_LOGIN:
            return {
                resetSuccess: false,
                resetFail: false
            }
        case CREATE_NEW_USER + FAIL:
            return {
                errorMessage: action.payload.response.data.error.message
            }
        default: 
            break;
 	} 
    return {
        ...state, 
    }
}

const persistConfig = {
    key: 'user',
    storage: storage,
    blacklist: ['errorMessage']
  };

  export default persistReducer(persistConfig, user);
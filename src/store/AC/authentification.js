import {
    RESET_PASSWORD,
    LOGIN,
    LOGOUT,
    CREATE_NEW_USER,
    CHANGE_PASSWORD,
    SET_TEMPORARY_TOKEN,
    UPDATE_USER,
    OPEN_LOGIN,
} from '../constants/authentification';

export const login = authData => ({ 
    type: LOGIN,
    payload: Object.assign( {}, authData, { requestMethod:'POST', requestPath: 'api/Customers/login?include=USER' })
});

export const logout = () => ({ 
    type: LOGOUT,
    payload: { requestMethod:'POST', requestPath: 'api/Customers/logout' } 
});


export const resetPwd = email => console.log(email) || ({ 
    type: RESET_PASSWORD,
    payload: Object.assign( {}, email, { requestMethod:'POST', requestPath: 'api/Customers/reset' }) 
});

export const createUser = userData => ({ 
    type: CREATE_NEW_USER,
    payload: Object.assign( {}, userData, { requestMethod:'POST', requestPath: 'api/Customers' }) 
});

export const changePassword = passwords => ({
    type: CHANGE_PASSWORD, 
    payload: Object.assign({}, passwords, {requestMethod: 'POST', requestPath: 'api/Customers/change-password' })
});

export const setTemporaryToken = token => ({
    type: SET_TEMPORARY_TOKEN,
    payload: token,
})

export const resetPassword = password => ({
    type: RESET_PASSWORD,
    payload: Object.assign({}, password, {requestMethod: 'POST', requestPath: 'api/Customers/reset-password' })
})

export const updateUserInfo = (userData, id) => ({
    type: UPDATE_USER,
    payload: Object.assign({}, userData, {requestMethod: 'PUT', requestPath: 'api/Customers/'+id })
})

export const openLogin = () => ({
    type: OPEN_LOGIN
})
import { 
    BILLING_CUSTOMER, 
    BILLING_INVOICE, 
    BILLING_CHARGED, 
    BILLING_ADD_SUBSCRIPTION, 
} from "../constants/billing";

export const setBillingCharged = (charged) => ({ type: BILLING_CHARGED, payload: charged });
export const setBillingCustomer = (customer) => ({ type: BILLING_CUSTOMER, payload: customer });
export const setBillingInvoice = (invoice) => ({ type: BILLING_INVOICE, payload: invoice });


export const billingAddSubscription = (subscription) => ({ 
    type: BILLING_ADD_SUBSCRIPTION,
    payload: Object.assign( {}, subscription, { requestMethod:'POST', requestPath: 'api/Subscriptions' })
});
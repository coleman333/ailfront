import Axios from "axios";

const invoicedApi = 'https://api.invoiced.com';
const invoicedKey = process.env.REACT_APP_INVOICED_KEY;

const invoiced = Axios.create({
    baseURL: invoicedApi
});

invoiced.interceptors.request.use(
  (config) => {
    const newConfig = config;
    newConfig.headers.Authorization = 'Basic ' +  btoa(invoicedKey+":");

    return newConfig;
  },
  (error) => {
    Promise.reject(error);
  },
);

export const createCustomer = async (customer) => {
  console.log(customer);
  const response = await invoiced.post('/customers', customer);
  console.log(response.data);
  
  return response.data;
}

export const createInvoice = async (data) => {
  const response = await invoiced.post('/invoices', data);
  console.log(response.data);

  return response.data;
}

export const sendInvoice = async (invoiceId) => {
  invoiced.post(`/invoices/${invoiceId}/emails`);
}
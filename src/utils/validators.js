export const emailValidator = (value) => {
    const reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    return reg.test(value)
 };

export const phoneValidator = (value) => {
    // const reg = /^\d{3}-\d{3}-\d{4}$/g;
    const reg = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
    return reg.test(value);
}

export const fieldNotEmpty = (value) => {
    return value.length>0;
}